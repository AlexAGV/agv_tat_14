package com.gomel.tat.home14.lib.feature.mail.screen;

import com.gomel.tat.home14.lib.util.Logger;

public class SentPage extends Page {

    public SentPage() {
        //super();
        Logger.debug("Sent page opening");
        this.gotoSent();
    }
}