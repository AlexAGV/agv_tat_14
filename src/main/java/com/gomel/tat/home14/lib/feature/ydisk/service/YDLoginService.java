package com.gomel.tat.home14.lib.feature.ydisk.service;

import com.gomel.tat.home14.lib.common.Account;
import com.gomel.tat.home14.lib.common.AccountBuilder;
import com.gomel.tat.home14.lib.feature.ydisk.screen.YDLoginPage;
import com.gomel.tat.home14.lib.util.Logger;

public class YDLoginService {

    public void loginToYandexDisk() {
        Account account = AccountBuilder.getDefaultAccount();
        YDLoginPage page = new YDLoginPage();
        Logger.info("Logging to YandexDisk with account '" + account.getLogin() + "'");
        page.open();
        page.login(account.getLogin(), account.getPassword());
    }
}
