package com.gomel.tat.home14.lib.feature.mail.screen;

import com.gomel.tat.home14.lib.feature.mail.Letter;
import com.gomel.tat.home14.lib.ui.Browser;
import com.gomel.tat.home14.lib.util.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebElement;

import java.util.List;

public abstract class Page {

    protected LettersManageForm lettersManageForm = new LettersManageForm();
    protected FolderSwitchForm folderSwitchForm = new FolderSwitchForm();

    private static final By DELETE_MAIL_BUTTON_LOCATOR = By.xpath("//a[@data-action='compose.delete']");
    private static final By DELETE_MAIL_MESSAGE_LOCATOR = By.xpath("//span[contains(text(),'1 сообщение удалено.')]");
    private static final String MAIL_LINK_LOCATOR_PATTERN = "//*[@class='block-messages']//a[contains(.,'%s')]";
    private static final String MAIL_ID_LOCATOR_PATTERN = "//a[contains(@href,'%s')]";
    private static final String MAIL_SUBJ_LOCATOR_PATTERN = ".//*[@title='%s']";

    public void compose() {
        lettersManageForm.compose();
    }

    public void gotoInbox() {
        folderSwitchForm.toInbox();
    }

    public void gotoSent() {
        folderSwitchForm.toSent();
    }

    public void gotoDraft() {
        folderSwitchForm.toDraft();
    }

    public void gotoTrash() {
        folderSwitchForm.toTrash();

    }

    public boolean isLetterPresent(Letter letter) {
        Integer found = 0;
        try {
            List<WebElement> search = Browser.current().getWrappedDriver().findElements(By.xpath(String.format(MAIL_LINK_LOCATOR_PATTERN, letter.getSubject())));
            found = search.size();
        if (0 == found) return false;
        for (WebElement elementN : search) if (elementN.isDisplayed()) return true;
        } catch (Exception e) { Logger.debug("!!! Exception in findElements() "+e.getMessage());}
        return false;
    }

    public boolean isLetterIDPresent(Letter letter) {
        Integer found = 0;
        try {
            List<WebElement> search = Browser.current().getWrappedDriver().findElements(By.xpath(String.format(MAIL_ID_LOCATOR_PATTERN, letter.getMessageID())));
            found = search.size();
            if (0 == found) return false;
            for (WebElement elementN : search) if (elementN.isDisplayed()) return true;
        } catch (Exception e) { Logger.debug("!!! Exception in findElements() "+e.getMessage());}
        return false;
    }

    public void deleteLetterBySubj(Letter letter) {
        Browser.current().click(By.xpath(String.format(MAIL_SUBJ_LOCATOR_PATTERN, letter.getSubject())));
        Browser.current().waitForElementIsClickable(DELETE_MAIL_BUTTON_LOCATOR);
        Browser.current().click(DELETE_MAIL_BUTTON_LOCATOR);
        Browser.current().waitForElementIsDisplayed(DELETE_MAIL_MESSAGE_LOCATOR);
    }
}
