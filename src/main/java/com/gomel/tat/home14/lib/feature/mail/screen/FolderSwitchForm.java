package com.gomel.tat.home14.lib.feature.mail.screen;

import com.gomel.tat.home14.lib.ui.Browser;
import org.openqa.selenium.By;

public class FolderSwitchForm {

    private static By inbox = By.xpath("//a[@href='#inbox']");
    private static By sent = By.xpath("//a[@href='#sent']");
    private static By draft = By.xpath("//a[@href='#draft']");
    private static By trash = By.xpath("//a[@href='#trash']");
    private static By spam = By.xpath("//a[@href='#spam']");

    public void toInbox() {
        Browser.current().doubleClick(inbox);
    }

    public void toSent() {
        Browser.current().doubleClick(sent);
    }

    public void toDraft() {
        Browser.current().doubleClick(draft);
    }

    public void toTrash() {
        Browser.current().doubleClick(trash);
    }
}