package com.gomel.tat.home14.lib.feature.ydisk;

import com.gomel.tat.home14.lib.common.CommonConstants;
import org.apache.commons.lang3.RandomStringUtils;

public class TestFileFactory {

    public static TestFile getTestFile() {
        String fileName = "test_file_" + RandomStringUtils.randomAlphanumeric(CommonConstants.TEST_FILENAME_LENGTH) + ".txt";
        String content = "test content " + RandomStringUtils.randomAlphanumeric(CommonConstants.TEST_CONTENT_LENGTH);
        return new TestFile(fileName, "", content);
    }

    public static TestFile[] getTestFiles(int qty) {
        TestFile[] testFileN = new TestFile[qty];
        for (int i = 0; i < qty; i++) {
            String fileName = "test_file_" + RandomStringUtils.randomAlphanumeric(CommonConstants.TEST_FILENAME_LENGTH) + ".txt";
            String content = "test content " + RandomStringUtils.randomAlphanumeric(CommonConstants.TEST_CONTENT_LENGTH);
            testFileN[i] = new TestFile(fileName, "", content);
        }
        return testFileN;
    }
}
