package com.gomel.tat.home14.lib.feature.mail.screen;

import com.gomel.tat.home14.lib.feature.mail.Letter;
import com.gomel.tat.home14.lib.ui.Browser;
import org.openqa.selenium.By;

public class ComposeForm {

    private static final By MAIL_TO = By.xpath("//*[@data-params='field=to']//ancestor::tr//input[@type='text']");
    private static final By SUBJ = By.id("compose-subj");
    private static final By CONTENT = By.id("compose-send");
    private static final By COMPOSE_SUBMIT_BUTTON = By.id("compose-submit");
    private static final By COMPOSE_CANCEL_BUTTON = By.xpath(".//*[@data-action='compose.close']");

    public void send(Letter letter) {
        createDraft(letter);
        Browser.current().click(COMPOSE_SUBMIT_BUTTON);
    }

    public void createDraft(Letter letter) {
        Browser.current().writeText(MAIL_TO, letter.getMailto());
        Browser.current().writeText(SUBJ, letter.getSubject());
        Browser.current().writeText(CONTENT, letter.getBody());
    }

    public void cancelCompose() {
        Browser.current().click(COMPOSE_CANCEL_BUTTON);
    }
}