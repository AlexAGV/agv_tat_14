package com.gomel.tat.home14.lib.feature.mail.screen;

import com.gomel.tat.home14.lib.ui.Browser;
import com.gomel.tat.home14.lib.util.Logger;
import org.openqa.selenium.By;

public class AutorisationForm {

    private static final By LOGIN_FIELD = By.name("login");
    private static final By PASSWORD_FIELD = By.name("passwd");
    private static final By SUBMIT_BUTTON = By.cssSelector("button[type='submit']");
    private static final By CHECKBOX = By.xpath(".//*[@class='_nb-checkbox-flag _nb-checkbox-normal-flag']");

    public void login(String login, String password) {
        Logger.debug("Loging in with " + login + "/" + password);
        Browser.current().click(CHECKBOX);
        Browser.current().clear(LOGIN_FIELD);
        Browser.current().writeText(LOGIN_FIELD, login);
        Browser.current().writeText(PASSWORD_FIELD, password);
        Browser.current().click(SUBMIT_BUTTON);
    }
}