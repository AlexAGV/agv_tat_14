package com.gomel.tat.home14.lib.util;

public interface IMdcContext {
    String REPORT_SUITE = "REPORT_SUITE";
    String REPORT_TEST = "REPORT_TEST";
    String REPORT_METHOD = "REPORT_METHOD";
}
