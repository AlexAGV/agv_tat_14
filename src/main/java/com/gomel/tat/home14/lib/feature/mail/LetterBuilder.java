package com.gomel.tat.home14.lib.feature.mail;

import com.gomel.tat.home14.lib.common.CommonConstants;
import com.gomel.tat.home14.lib.util.Logger;
import org.apache.commons.lang3.RandomStringUtils;

public class LetterBuilder {

    public static Letter getTestLetter() {
        Logger.debug("Building 'default' test letter");
        Letter letter = new Letter();
        letter.setMailto(CommonConstants.DEFAULT_MAIL_TO_SEND);
        letter.setSubject("Test subject " + RandomStringUtils.randomAlphanumeric(CommonConstants.TEST_SUBJECT_LENGTH));
        letter.setBody("Test content " + RandomStringUtils.randomAlphanumeric(CommonConstants.TEST_CONTENT_LENGTH));
        return letter;
    }

    public static Letter getLetterWithWrongAddress() {
        Letter letter = getTestLetter();
        Logger.debug("Changing mailto address to invalid");
        letter.setMailto("NotAMailAddress");
        return letter;
    }

    public static Letter getLetterWithoutAddress() {
        Letter letter = getTestLetter();
        letter.setMailto("");
        return letter;
    }

    public static Letter getLetterWithoutSubj() {
        Letter letter = getTestLetter();
        Logger.debug("Changing subject to empty");
        letter.setSubject("");
        return letter;
    }

    public static Letter getLetterWithoutBody() {
        Letter letter = getTestLetter();
        Logger.debug("Changing letter body to empty");
        letter.setBody("");
        return letter;
    }
}
