package com.gomel.tat.home14.lib.common;

import com.gomel.tat.home14.lib.util.Logger;
import org.apache.commons.lang3.RandomStringUtils;

public class AccountBuilder {

    public static Account getDefaultAccount() {
        Logger.info("Building 'default' account");
        Account account = new Account();
        account.setLogin(CommonConstants.DEFAULT_MAIL_USER_LOGIN);
        account.setPassword(CommonConstants.DEFAULT_MAIL_USER_PASSWORD);
        account.setEmail(CommonConstants.DEFAULT_MAIL_TO_SEND);
        return account;
    }

    public static Account getAccountWithInvalidPassword() {
        Account account = getDefaultAccount();
        Logger.info("Changing password to 'invalid'");
        account.setPassword(account.getPassword() + RandomStringUtils.randomAlphabetic(5));
        return account;
    }
}
