package com.gomel.tat.home14.lib.feature.ydisk.service;

import com.gomel.tat.home14.lib.feature.ydisk.TestFile;
import com.gomel.tat.home14.lib.feature.ydisk.screen.YandexDiskPage;
import com.gomel.tat.home14.lib.ui.Browser;
import com.gomel.tat.home14.lib.util.Logger;
import org.openqa.selenium.By;

import java.io.IOException;

public class DiskService {
    YandexDiskPage page = new YandexDiskPage();

    public void uploadFile(TestFile testFile) {
        Logger.info("Uploading file " + testFile.getTestFileName());
        page.uploadFile(testFile);
        pause();
    }

    public void downloadFile(TestFile testFile) {
        Logger.info("Downloading file " + testFile.getTestFileName());
        page.downloadFile(By.xpath("//*[@title='" + testFile.getTestFileName() + "']"));
        pause();
    }

    public boolean isTestFilePresent(TestFile testFile) {
        Logger.info("Checking if presents file " + testFile.getTestFileName());
        return page.isTestFilePresent(testFileLocator(testFile));
    }

    public boolean isTestFileDownloaded(TestFile testFile) {
        Logger.info("Checking if downloaded file " + testFile.getTestFileName());
        boolean fileDownloaded = false;
        try {
            fileDownloaded = TestFile.isTestFileStored(testFile);
        } catch (IOException e) {
        }
        return fileDownloaded;
    }

    public void moveToTrash(TestFile testFile) {
        Logger.info("Moving to trash bin file " + testFile.getTestFileName());
        page.moveToTrash(testFileLocator(testFile));
        pause();pause();
    }

    public void selectMany(TestFile testFile) {
        Logger.info("Selecting file " + testFile.getTestFileName());
        page.selectMany(testFileLocator(testFile));
    }

    public void restoreFromTrash(TestFile testFile) {
        Logger.info("Restoring from trash bin file " + testFile.getTestFileName());
        page.restoreFromTrash(testFileLocator(testFile));
        pause();
    }

    public void permanentDelete(TestFile testFile) {
        Logger.info("Deleting from trash bin file " + testFile.getTestFileName());
        page.permanentDelete(testFileLocator(testFile));
        pause();
    }

    public void openTrashBin() {
        Logger.info("Opening Trash bin");
        page.openTrashBin();
        pause();
    }

    public void openRootFolder() {
        Logger.info("Opening Root folder");
        page.openRootFolder();
        pause();
    }

    private By testFileLocator(TestFile testFile) {
        return By.xpath(".//*[@title='" + testFile.getTestFileName() + "']");
    }

    public void waitForDeletingSomeFiles(int howManyFiles) {
    By locator=By.xpath("//*[contains(text(),'"+howManyFiles+" Файл']");
        Browser.current().waitForElementIsDisplayed(locator);
    }


    private void pause() {
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
        }
    }
}
