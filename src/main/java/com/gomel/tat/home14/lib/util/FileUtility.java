package com.gomel.tat.home14.lib.util;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;

public class FileUtility {

    public static final String TEMP_DIR = createTempDir();
    public static final String TEMP_DIR_NAME = "TMP";

    private static String createTempDir() {
        Logger.info("Creating temporary dir for tests");
        boolean success = (new File(TEMP_DIR_NAME)).mkdirs();
        if (!success) {
            Logger.error("Can't create temp dir!");
            System.exit(1);
        }
        String dir = new File(FileUtility.TEMP_DIR_NAME).getAbsolutePath();
        Logger.info("Temporary dir for downloads/upploads set to " + dir);
        return dir;
    }

    public static void deleteTempDir() {
        Logger.info("Deleting temporary dir for tests");
        try {
            FileUtils.deleteDirectory(new File(TEMP_DIR));
        } catch (IOException e) {
            Logger.error("Can't delete temp_dir!");
        }
    }

}
