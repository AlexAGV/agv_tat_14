package com.gomel.tat.home14.lib.runner;


import com.gomel.tat.home14.lib.ui.Browser;
import com.gomel.tat.home14.lib.util.Logger;
import org.testng.ITestResult;
import org.testng.TestListenerAdapter;

public class TestListener extends TestListenerAdapter {


    @Override
    public void onTestFailure(ITestResult result) {
        Logger.debug("=========================================================");
        Logger.error("Test fail: ", result.getThrowable());
        Browser.current().screenshot();
        Logger.debug("=========================================================");
    }

    @Override
    public void onTestStart(ITestResult result) {
        Logger.info("--------------------------------------------------------");
        Logger.info("Test start: " + result.getName());
        Logger.info("( thread: " + Thread.currentThread()+")");
        Logger.info("--------------------------------------------------------");
    }

    @Override
    public void onTestSuccess(ITestResult result) {
        Logger.info("_________________________________________________________");
        Logger.info("Test pass: " + result.getName());
        Logger.info("_________________________________________________________");
    }


}