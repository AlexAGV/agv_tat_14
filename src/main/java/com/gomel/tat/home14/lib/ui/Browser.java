package com.gomel.tat.home14.lib.ui;

import com.gomel.tat.home14.lib.config.GlobalConfig;
import com.gomel.tat.home14.lib.util.FileUtility;
import com.gomel.tat.home14.lib.util.Logger;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.internal.WrapsDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.xml.XmlSuite;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;


public class Browser implements WrapsDriver {

    public static final int DRIVER_PAGE_LOAD_TIMEOUT_SECONDS = 5;
    public static final int DRIVE_IMPL_WAIT_TIMEOUT_SECONDS = 5;
    public static final int COMMON_ELEMENT_WAIT_TIME_OUT = 5;
    private WebDriver driver;
    private static final Thread ONLY_THREAD = Thread.currentThread();
    private static Map<Thread, Browser> instances = Collections.synchronizedMap(new HashMap<Thread, Browser>());

    private Browser() {
    }

    public WebDriver getWrappedDriver() {
        return driver;
    }

    public static synchronized Browser rise() {
        Logger.info("Rising browser " + GlobalConfig.config().getBrowserType());
        if (browserForThread() != null) {
            return browserForThread();
            //browserForThread().quit();
        }
        Browser browser = new Browser();
        browser.createDriver();
        instances.put(isNotParallel() ? ONLY_THREAD : Thread.currentThread(), browser);
        return browser;
    }

    public synchronized static boolean isBrowserOpened() {
        Browser browser = browserForThread();
        return browser != null && browser.getWrappedDriver() != null;
    }

    public static synchronized Browser current() {
        if (isBrowserOpened()) {
            return browserForThread();
        }
        return null;
    }

    private static boolean isNotParallel() {
        return ((GlobalConfig.config().getParallelMode()) == XmlSuite.ParallelMode.FALSE);
    }

    private static Browser browserForThread() {
        return (isNotParallel()) ? instances.get(ONLY_THREAD) : instances.get(Thread.currentThread());
    }

    public void quit() {
        screenshot();
        Logger.debug("Stop browser");
        try {
            WebDriver wrappedDriver = getWrappedDriver();
            if (wrappedDriver != null) {
                wrappedDriver.quit();
            }
        } catch (Exception ignore) {
        } finally {
            instances.remove(isNotParallel() ? ONLY_THREAD : Thread.currentThread());
        }
    }

    public void openURL(String url) {
        Logger.debug("Opening page : " + url);
        getWrappedDriver().get(url);
        screenshot();
    }

    private static DesiredCapabilities chromeCapabilities() {
        DesiredCapabilities cap = DesiredCapabilities.chrome();
        ChromeOptions options = new ChromeOptions();
        Map<String, Object> content_setting = new HashMap<>();
        content_setting.put("automatic_downloads", 1);
        content_setting.put("popups", 1);
        Map<String, Object> prefs = new HashMap<>();
        prefs.put("download.default_directory", FileUtility.TEMP_DIR);
        prefs.put("download.prompt_for_download", "false");
        prefs.put("profile.default_content_setting_values", content_setting);
        options.setExperimentalOption("prefs", prefs);
        options.addArguments("--disable-web-security");
        options.addArguments("--allow-running-insecure-content");
        cap.setCapability(ChromeOptions.CAPABILITY, options);
        Logger.debug("Set Chrome browser profile");
        return cap;
    }

    private static DesiredCapabilities firefoxCapabilities() {
        DesiredCapabilities cap = DesiredCapabilities.firefox();
        FirefoxProfile profile = new FirefoxProfile();
        profile.setPreference("browser.helperApps.alwaysAsk.force", false);
        profile.setPreference("browser.download.folderList", 2);
        profile.setPreference("browser.download.dir", FileUtility.TEMP_DIR);
        profile.setPreference("browser.download.manager.showWhenStarting", false);
        profile.setPreference("services.sync.prefs.sync.browser.download.manager.showWhenStarting", false);
        profile.setPreference("browser.download.useDownloadDir", true);
        profile.setPreference("browser.helperApps.neverAsk.saveToDisk", "text/plain");
        profile.setPreference("browser.privatebrowsing.dont_prompt_on_enter", true);
        profile.setPreference("startup.homepage_welcome_url", "");
        profile.setPreference("startup.homepage_welcome_url.additional", "");
        profile.setEnableNativeEvents(true);
        cap.setCapability("firefox_profile", profile);
        Logger.debug("Set Firefox browser profile");
        return cap;
    }

    private static WebDriver chromeLocal() {
        return new ChromeDriver(chromeCapabilities());
    }

    private static WebDriver chromeRemote() throws MalformedURLException {
        // java -jar selenium-server-standalone-2.45.0.jar -port 4444 -Dwebdriver.chrome.driver="d:\path\to\chromedriver.exe"
        //http://127.0.0.1:4444/wd/hub
        System.setProperty("webdriver.chrome.driver", "./chromedriver/chromedriver.exe");
        RemoteWebDriver driver=null;
        try {
            driver = new RemoteWebDriver(new URL(GlobalConfig.config().getSeleniumHub()), chromeCapabilities());
        } catch (MalformedURLException e) {
            Logger.error("Can't get Webdriver!");System.exit(1);
        }
        return driver;
    }

    private static WebDriver firefoxLocal() {
        return new FirefoxDriver(firefoxCapabilities());
    }

    private static WebDriver firefoxRemote() throws MalformedURLException {
        // java -jar selenium-server-standalone-2.45.0.jar -port 4444
        return new RemoteWebDriver(new URL(GlobalConfig.config().getSeleniumHub()), firefoxCapabilities());
    }

    private static void standAloneServerLaunch(BrowserType browser) {
        Logger.info("Running standalone server for " + browser);
        String[] cmd;
        switch (browser) {
            default:
            case FIREFOX:
                cmd = new String[]{"java", "-jar", "./src/main/resources/selenium-standalone-server/selenium-server-standalone-2.48.2.jar", "-port4444"};
                break;
            case CHROME:
                cmd = new String[]{"java", "-jar", "./src/main/resources/selenium-standalone-server/selenium-server-standalone-2.48.2.jar", "-port4444", "-Dwebdriver.chrome.driver=./src/main/resources/chromedriver/chromedriver"};
                break;
        }
        try {
            Runtime.getRuntime().exec(cmd);
        } catch (Exception e) {
            System.err.println("Problem with launching standalone-server: " + e.getMessage());
        }
    }

    private void createDriver() {
        this.driver = null;
        BrowserType selectedBrowser = GlobalConfig.config().getBrowserType();
        //if (isRemote()) {standAloneServerLaunch(selectedBrowser);}
        try {
            switch (selectedBrowser) {
                case CHROME:
                    this.driver = isRemote() ? chromeRemote() : chromeLocal();
                    break;
                case FIREFOX:
                    this.driver = isRemote() ? firefoxRemote() : firefoxLocal();
                    break;
            }
        } catch (MalformedURLException e) {
            Logger.error("Problem with launching driver: ", e);
        }
        driver.manage().window().maximize();
        driver.manage().timeouts().pageLoadTimeout(DRIVER_PAGE_LOAD_TIMEOUT_SECONDS, TimeUnit.SECONDS);
        driver.manage().timeouts().implicitlyWait(DRIVE_IMPL_WAIT_TIMEOUT_SECONDS, TimeUnit.SECONDS);
    }

    private boolean isRemote() {
        String seleniumHub = GlobalConfig.config().getSeleniumHub();
        return seleniumHub != null && seleniumHub != "";
    }

    public void waitForElementIsClickable(By locator) {
        Logger.debug("Waiting for clicable webelement " + locator);
        try {
            new WebDriverWait(getWrappedDriver(), COMMON_ELEMENT_WAIT_TIME_OUT).until(ExpectedConditions.elementToBeClickable(locator));
        } catch (TimeoutException e) {
            Logger.debug("Time out!");
         //   pause();
        }
    //    screenshot();
    }

    public void waitForElementIsDisplayed(By locator) {
        Logger.debug("Waiting for displaying webelement " + locator);
        try {
            new WebDriverWait(getWrappedDriver(), COMMON_ELEMENT_WAIT_TIME_OUT).until(ExpectedConditions.visibilityOfElementLocated(locator));
        } catch (TimeoutException e) {
            Logger.debug("Time out!");
          //  pause();
        }
     //   screenshot();
    }

    public void waitForDisappear(By locator) {
        Logger.debug("Waiting for disappearing webelement " + locator);
        try {
            new WebDriverWait(getWrappedDriver(), COMMON_ELEMENT_WAIT_TIME_OUT)
                    .until(ExpectedConditions.invisibilityOfElementLocated(locator));
        } catch (TimeoutException e) {
            Logger.debug("Time out!");
          //  pause();
        }
    //    screenshot();
    }

    public static void screenshot() {
        WebDriver driver = current().getWrappedDriver();
        if (isBrowserOpened()) {
            try {
                byte[] screenshotBytes = ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
                File screenshotFile = new File(screenshotName());
                FileUtils.writeByteArrayToFile(screenshotFile, screenshotBytes);
                Logger.save(screenshotFile.getName());
            } catch (Exception e) {
                Logger.error("Failed to write screenshot: " + e.getMessage(), e);
            }
        }
    }

    private static String screenshotName() {
        return GlobalConfig.config().getResultDir() + File.separator + "screen_" + System.nanoTime() + ".png";
    }

    public void waitForAppear(By locator) {
        Logger.debug("Waiting for webelement: " + locator);
        new WebDriverWait(getWrappedDriver(), COMMON_ELEMENT_WAIT_TIME_OUT).until(ExpectedConditions.presenceOfElementLocated(locator));
    //    screenshot();
    }

    public void writeText(By locator, String text) {
        Logger.debug("Writing text to : " + locator + " text = '" + text + "'");
        //new Actions(getWrappedDriver()).sendKeys(getWrappedDriver().findElement(locator),text).perform();
        getWrappedDriver().findElement(locator).sendKeys(text);
     //   screenshot();
    }

    public void writeTextByAction(By locator, String text) {
        Logger.debug("Writing text to : " + locator + " text = '" + text + "'");
        new Actions(getWrappedDriver()).sendKeys(getWrappedDriver().findElement(locator), text).perform();
    //    screenshot();
    }

    public void click(By locator) {
        Logger.debug("Clicking on webelement: " + locator);
        new Actions(getWrappedDriver()).click(getWrappedDriver().findElement(locator)).perform();
        screenshot();
    }

    public void doubleClick(By locator) {
        Logger.debug("Clicking twice on webelement: " + locator);
        new Actions(getWrappedDriver()).doubleClick(getWrappedDriver().findElement(locator)).perform();
      //  screenshot();
    }


    public String getText(By locator) {
        Logger.debug("Getting text of webelement: " + locator);
    //    screenshot();
        return getWrappedDriver().findElement(locator).getText();
    }

    public void clear(By locator) {
        Logger.debug("Clearing of webelement: " + locator);
        getWrappedDriver().findElement(locator).clear();
    //    screenshot();
    }

    public void submit(By locator) {
        Logger.debug("Submitting of webelement: " + locator);
        getWrappedDriver().findElement(locator).submit();
        screenshot();
    }

    public void dragTo(By draggable, By target) {
        new Actions(driver).dragAndDrop(getWrappedDriver().findElement(draggable), getWrappedDriver().findElement(target)).perform();
        screenshot();
    }

    public void clickWithCTRL(By locator) {
        Logger.debug("Clicking with hold CTRL key of webelement: " + locator);
        Actions action = new Actions(getWrappedDriver());
        action.keyDown(Keys.CONTROL).click(getWrappedDriver().findElement(locator)).build().perform();
        screenshot();
    }

    private void pause() {
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
        }
    }
}