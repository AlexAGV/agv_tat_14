package com.gomel.tat.home14.lib.feature.mail.screen;

import com.gomel.tat.home14.lib.common.Account;
import com.gomel.tat.home14.lib.ui.Browser;
import com.gomel.tat.home14.lib.util.Logger;
import org.openqa.selenium.By;

public class MailLoginPage {
    private AutorisationForm autorisationForm = new AutorisationForm();

    public static final By ERROR_ON_LOGIN_LOCATOR = By.xpath("//*[@class='error-msg']");
    public static final By INBOX_LINK_LOCATOR = By.xpath("//*[@href='#inbox']");

    public static final String MAILBOX_URL = "http://mail.yandex.by";
    public static final By DROPDOWN_ACCOUNT_MENU_LOCATOR = By.xpath("//*[@class='header-user-name js-header-user-name']");
    public static final By DROPDOWN_LOGOFF_LOCATOR = By.xpath("//*[@data-metric='Меню сервисов:Выход']");

    public MailLoginPage() {
        Logger.debug("MainLoginPage");
        Browser.current().openURL(MAILBOX_URL);
    }

    public void login(Account account) {
        autorisationForm.login(account.getLogin(), account.getPassword());
    }

    public boolean isLoginSuccessful() {
        Logger.debug("Searching for webelement " + INBOX_LINK_LOCATOR);
        return !(0 == Browser.current().getWrappedDriver().findElements(INBOX_LINK_LOCATOR).size());
    }

    public boolean isLoginError() {
        Logger.debug("Searching for webelement " + ERROR_ON_LOGIN_LOCATOR);
        return !(0 == Browser.current().getWrappedDriver().findElements(ERROR_ON_LOGIN_LOCATOR).size());
    }

    public void logOff() {
        Logger.info("Logging off from account ");
        Browser.current().click(DROPDOWN_ACCOUNT_MENU_LOCATOR);
        Browser.current().click(DROPDOWN_LOGOFF_LOCATOR);
        Browser.current().waitForDisappear(DROPDOWN_ACCOUNT_MENU_LOCATOR);
        Browser.current().openURL(MAILBOX_URL);
    }
}
