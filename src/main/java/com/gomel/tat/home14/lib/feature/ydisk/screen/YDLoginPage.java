package com.gomel.tat.home14.lib.feature.ydisk.screen;

import com.gomel.tat.home14.lib.ui.Browser;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;

public class YDLoginPage {

    private static final String YANDEX_DISK_BASE_URL = "https://disk.yandex.ru/";
    private static final By LOGIN_INPUT_LOCATOR = By.name("login");
    private static final By PASSWORD_INPUT_LOCATOR = By.name("password");
    private static final By SUBMIT_BUTTON = By.xpath("//button[@type='submit']");
    private static final By ADW_WINDOW = By.xpath("//div[contains(text(),'Экономьте время')]");
    private static final By ADW_CLOSE = By.name("//a[@class='_nb-popup-close js-dialog-close']");


    public void open() {
        Browser.current().openURL(YANDEX_DISK_BASE_URL);
    }

    public YandexDiskPage login(String userLogin, String userPassword) {

        Browser.current().clear(LOGIN_INPUT_LOCATOR);
        Browser.current().writeTextByAction(LOGIN_INPUT_LOCATOR, userLogin);
        //Browser.current().clear(PASSWORD_INPUT_LOCATOR);
        Browser.current().writeTextByAction(PASSWORD_INPUT_LOCATOR, userPassword);
        Browser.current().submit(PASSWORD_INPUT_LOCATOR);
        //Browser.current().click(SUBMIT_BUTTON);

     try {
         String adw=Browser.current().getText(ADW_WINDOW);
         if (adw!=null) Browser.current().click(ADW_CLOSE);
     } catch (NoSuchElementException e) {}


        Browser.current().waitForDisappear(PASSWORD_INPUT_LOCATOR);
        return new YandexDiskPage();
    }
}