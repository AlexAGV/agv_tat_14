package com.gomel.tat.home14.lib.feature.mail.service;

import com.gomel.tat.home14.lib.common.Account;
import com.gomel.tat.home14.lib.common.AccountBuilder;
import com.gomel.tat.home14.lib.feature.mail.screen.MailLoginPage;
import com.gomel.tat.home14.lib.util.Logger;

public class LoginService {

      public void loginToMailBox() {
        Account account = AccountBuilder.getDefaultAccount();
        MailLoginPage page = new MailLoginPage();
        Logger.info("Logging with account '"+account.getLogin()+"'");
        page.login(account);
    }

    public boolean checkLoginSuccessfull(Account account) {
        MailLoginPage page = new MailLoginPage();
        Logger.info("Logging with account '"+account.getLogin()+"'");
        page.login(account);
        Logger.info("Checking successfull login");
        return page.isLoginSuccessful();
    }

    public boolean checkErrorWithWrongLogin(Account account) {
        MailLoginPage page = new MailLoginPage();
        if (page.isLoginSuccessful()) {page.logOff();}
        Logger.info("Logging with account '"+account.getLogin()+"'");
        page.login(account);
        Logger.info("Checking error message");
        return page.isLoginError();
    }

}
