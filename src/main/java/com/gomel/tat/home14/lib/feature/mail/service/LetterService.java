package com.gomel.tat.home14.lib.feature.mail.service;

import com.gomel.tat.home14.lib.feature.mail.Letter;
import com.gomel.tat.home14.lib.feature.mail.screen.ComposePage;
import com.gomel.tat.home14.lib.feature.mail.screen.Page;
import com.gomel.tat.home14.lib.ui.Browser;
import com.gomel.tat.home14.lib.util.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.WebDriverWait;

public class LetterService {
    private ComposePage composePage;

    public void sendLetter(Letter letter) {
        composePage = new ComposePage();
        Logger.info("Sending letter '" + letter.getSubject() + "'");
        composePage.sendLetter(letter);
        new WebDriverWait(Browser.current().getWrappedDriver(), 5, 1000);
    }

    public String sendLetterReturnId(Letter letter) {
        composePage = new ComposePage();
        Logger.info("Return sending letter ID");
        return composePage.sendLetterReturnId(letter);
    }

    public void createDraft(Letter letter) {
        composePage = new ComposePage();
        Logger.info("Creating draft '" + letter.getSubject() + "'");
        composePage.createDraft(letter);
    }

    public void deleteLetter(Page page, Letter letter) {
        Logger.info("Deleting letter '" + letter.getSubject() + "'");
        page.deleteLetterBySubj(letter);
        new WebDriverWait(Browser.current().getWrappedDriver(), 6, 1000);
    }

    public void cancelCompose() {
        composePage.cancelCompose();
    }

    public boolean isLetterInFolder(Page page, Letter letter) {
        return page.isLetterPresent(letter);
    }

    public boolean isLetterInFolderWithId(Page page, Letter letter) {
        return page.isLetterIDPresent(letter);
    }

    public boolean isErrorWhileSend() {
        return composePage.isErrorSendingLetter();
    }

    public void waitForIncoming() {Browser.current().waitForDisappear(By.xpath("//*[contains(text(),'Получено новое письмо']"));}

}
