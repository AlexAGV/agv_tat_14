package com.gomel.tat.home14.lib.feature.mail.screen;

import com.gomel.tat.home14.lib.util.Logger;

public class DraftPage extends Page {

    public DraftPage() {
        super();
        Logger.debug("Draft page opening ");
        this.gotoDraft();
    }
}