package com.gomel.tat.home14.lib.runner;

import com.gomel.tat.home14.lib.ui.Browser;
import org.testng.ITestContext;
import org.testng.TestListenerAdapter;

public class BrowserQuitListener extends TestListenerAdapter {

    @Override
    public void onFinish(ITestContext testContext) {
        super.onFinish(testContext);
        if (Browser.current() != null) {
      //      Browser.current().quit();
        }
    }
}
