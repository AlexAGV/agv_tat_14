package com.gomel.tat.home14.lib.feature.mail.screen;

import com.gomel.tat.home14.lib.feature.mail.Letter;
import com.gomel.tat.home14.lib.ui.Browser;
import org.openqa.selenium.By;

public class TrashPage extends Page {

    private static final By DELETE_PERMANENT_BUTTON_LOCATOR = By.xpath("//a[@data-action='delete']");
    private static final By DELETE_PERMANENT_MESSAGE_LOCATOR = By.xpath("//span[contains(text(),'1 сообщение удалено.')]");
    private static final String MAIL_SUBJ_LOCATOR_PATTERN = ".//*[@title='%s']";

    public TrashPage() {
        this.gotoTrash();
    }

    public void deleteLetterBySubj(Letter letter) {
        Browser.current().click(By.xpath(String.format(MAIL_SUBJ_LOCATOR_PATTERN, letter.getSubject())));
        Browser.current().waitForElementIsClickable(DELETE_PERMANENT_BUTTON_LOCATOR);
        Browser.current().click(DELETE_PERMANENT_BUTTON_LOCATOR);
        Browser.current().waitForElementIsDisplayed(DELETE_PERMANENT_MESSAGE_LOCATOR);
    }
}