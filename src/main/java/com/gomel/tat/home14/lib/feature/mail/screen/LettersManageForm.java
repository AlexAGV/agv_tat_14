package com.gomel.tat.home14.lib.feature.mail.screen;

import com.gomel.tat.home14.lib.ui.Browser;
import org.openqa.selenium.By;

public class LettersManageForm {

    private static final By COMPOSE_BUTTON = By.xpath("//a[@href='#compose']");
    private static final By DELETE_BUTTON = By.xpath("//a[contains(@class,'__item_delete')]");

    public void compose() {
        Browser.current().click(COMPOSE_BUTTON);
    }
}