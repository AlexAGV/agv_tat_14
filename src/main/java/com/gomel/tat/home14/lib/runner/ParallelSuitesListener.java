package com.gomel.tat.home14.lib.runner;

import com.gomel.tat.home14.lib.config.GlobalConfig;
import com.gomel.tat.home14.lib.util.Logger;
import org.testng.ISuite;
import org.testng.ISuiteListener;

public class ParallelSuitesListener implements ISuiteListener {

    @Override
    public void onStart(ISuite suite) {
        Logger.info("Suite starts: " + suite.getName());
        //Logger.info("Parallel mode set to "+GlobalConfig.config().getParallelMode());
        suite.getXmlSuite().setParallel(GlobalConfig.config().getParallelMode());
        //Logger.info("Thread count set to "+GlobalConfig.config().getThreadCount());
        suite.getXmlSuite().setThreadCount(GlobalConfig.config().getThreadCount());
    }

    @Override
    public void onFinish(ISuite suite) {
        Logger.info("Suite finished: " + suite.getName());
    }
}
