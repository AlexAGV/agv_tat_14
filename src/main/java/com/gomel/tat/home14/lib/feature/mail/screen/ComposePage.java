package com.gomel.tat.home14.lib.feature.mail.screen;

import com.gomel.tat.home14.lib.feature.mail.Letter;
import com.gomel.tat.home14.lib.ui.Browser;
import com.gomel.tat.home14.lib.util.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.List;

public class ComposePage extends Page {
    private ComposeForm composeForm = new ComposeForm();

    private static final By SAVE_DIALOG_POPUP_LOCATOR = By.xpath(".//p[text()='Сохранить сделанные изменения?']");
    private static final By SAVE_DIALOG_SAVE_BUTTON_LOCATOR = By.xpath(".//*[@data-action='dialog.save']");
    private static final By SEND_MAIL_BUTTON_LOCATOR = By.id("compose-submit");
    private static final By YOU_HAVE_GOT_MAIL_MESSAGE = By.xpath("//a[contains(text(),'от Александр Г.')]");
    private static final By SAVE_DIALOG_CANCEL_BUTTON_LOCATOR = By.xpath(".//*[@data-action='dialog.dont_save']");
    private static final By RECEIVE_SENDED_LETTER_LOCATOR = By.xpath("//a[contains(text(),'от Александр Г')]");
    private static final By INVALID_ADDRESS_MESSAGE_LOCATOR = By.xpath("//*[contains(text(),'Некорректный адрес электронной почты')]");

    public ComposePage() {
        Logger.debug("Compose page opening ");
        this.compose();
    }

    public void sendLetter(Letter letter) {
        composeForm.send(letter);
        if (!isErrorSendingLetter()) Browser.current().waitForDisappear(SEND_MAIL_BUTTON_LOCATOR);
        Browser.screenshot();
    }

    public String sendLetterReturnId(Letter letter) {
        String messageID[], href;
        composeForm.send(letter);
        Browser.current().waitForElementIsDisplayed(YOU_HAVE_GOT_MAIL_MESSAGE);
        WebElement recievedMessage = Browser.current().getWrappedDriver().findElement(RECEIVE_SENDED_LETTER_LOCATOR);
        href = recievedMessage.getAttribute("href");
        messageID = href.split("#message/");
        return messageID[1];
    }

    public void cancelCompose() {
        composeForm.cancelCompose();
        Browser.current().waitForElementIsDisplayed(SAVE_DIALOG_POPUP_LOCATOR);
        Browser.current().click(SAVE_DIALOG_CANCEL_BUTTON_LOCATOR);
    }

    public void createDraft(Letter letter) {
        composeForm.createDraft(letter);
        gotoDraft();
        Browser.current().waitForElementIsDisplayed(SAVE_DIALOG_POPUP_LOCATOR);
        Browser.current().click(SAVE_DIALOG_SAVE_BUTTON_LOCATOR);
        Browser.current().waitForDisappear(SEND_MAIL_BUTTON_LOCATOR);
    }

    public boolean isErrorSendingLetter() {
        List<WebElement> search = Browser.current().getWrappedDriver().findElements(INVALID_ADDRESS_MESSAGE_LOCATOR);
        int i = search.size();
        if (0 == i) return false;
        for (WebElement elementN : search) if (elementN.isDisplayed()) return true;
        return false;
    }
}
