package com.gomel.tat.home14.tests.mail;

import com.gomel.tat.home14.lib.feature.mail.Letter;
import com.gomel.tat.home14.lib.feature.mail.LetterBuilder;
import com.gomel.tat.home14.lib.feature.mail.screen.InboxPage;
import com.gomel.tat.home14.lib.feature.mail.screen.SentPage;
import org.testng.Assert;
import org.testng.annotations.Test;

public class SendLetterWithoutContentTest extends BaseMailTest {

    @Test(description = "Sent a letter without content and check that it appears in Sent and Inbox folders", timeOut = 15000)
    public void SendLetterWithoutContentTest() {
        Letter letter = LetterBuilder.getLetterWithoutBody();
        letterService.sendLetter(letter);
        Assert.assertTrue(letterService.isLetterInFolder(new SentPage(), letter));
        Assert.assertTrue(letterService.isLetterInFolder(new InboxPage(), letter));
    }
}
