package com.gomel.tat.home14.tests.mail;

import com.gomel.tat.home14.lib.feature.mail.Letter;
import com.gomel.tat.home14.lib.feature.mail.LetterBuilder;
import com.gomel.tat.home14.lib.feature.mail.screen.DraftPage;
import com.gomel.tat.home14.lib.feature.mail.screen.TrashPage;
import org.testng.Assert;
import org.testng.annotations.*;

public class CreateAndDeleteDraftTest  extends BaseMailTest {

    @Test(description = "Create a draft, check that it appears in Draft folder, delete it check that it appears in Trash folder", timeOut = 15000)
    public void CreateAndDeleteDraftTest() {
        Letter letter = LetterBuilder.getTestLetter();
        letterService.createDraft(letter);
        Assert.assertTrue(letterService.isLetterInFolder(new DraftPage(), letter));
        letterService.deleteLetter(new DraftPage(), letter);
        Assert.assertTrue(letterService.isLetterInFolder(new TrashPage(), letter));
    }
}

