package com.gomel.tat.home14.tests.login;

import com.gomel.tat.home14.lib.ui.Browser;
import com.gomel.tat.home14.lib.util.Logger;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;

public class BaseLoginTest {

    @BeforeSuite
    public void prepeareForTests() {
        if (!Browser.isBrowserOpened()) Browser.rise();
    }

    @AfterSuite
    public void closeWebdriver() {
        Logger.debug("NOT Closing browser after Login tests");
    //    Browser.current().quit();
    }
}
