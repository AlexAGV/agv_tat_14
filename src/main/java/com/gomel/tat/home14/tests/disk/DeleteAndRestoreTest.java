package com.gomel.tat.home14.tests.disk;

import com.gomel.tat.home14.lib.feature.ydisk.TestFile;
import com.gomel.tat.home14.lib.feature.ydisk.TestFileFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

public class DeleteAndRestoreTest extends BaseYDiskTest {

    TestFile testFile = TestFileFactory.getTestFile();

    @Test
    public void DeleteAndRestoreTest() {
        testFile.storeTestFile();
        diskService.openRootFolder();
        diskService.uploadFile(testFile);
        testFile.deleteStoredTestFile();
        diskService.moveToTrash(testFile);
        diskService.openTrashBin();
        diskService.restoreFromTrash(testFile);
        diskService.openRootFolder();
        Assert.assertTrue(diskService.isTestFilePresent(testFile));
    }
}
