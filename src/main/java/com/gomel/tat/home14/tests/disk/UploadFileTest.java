package com.gomel.tat.home14.tests.disk;

import com.gomel.tat.home14.lib.feature.ydisk.TestFile;
import com.gomel.tat.home14.lib.feature.ydisk.TestFileFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

public class UploadFileTest extends BaseYDiskTest {

    TestFile testFile = TestFileFactory.getTestFile();

    @Test
    public void UploadFileTest() {
        testFile.storeTestFile();
        diskService.openRootFolder();
        pause();
        diskService.uploadFile(testFile);
        Assert.assertTrue(diskService.isTestFilePresent(testFile));
        testFile.deleteStoredTestFile();
    }
}
