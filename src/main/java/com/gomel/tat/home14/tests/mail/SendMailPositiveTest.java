package com.gomel.tat.home14.tests.mail;

import com.gomel.tat.home14.lib.feature.mail.Letter;
import com.gomel.tat.home14.lib.feature.mail.LetterBuilder;
import com.gomel.tat.home14.lib.feature.mail.screen.InboxPage;
import com.gomel.tat.home14.lib.feature.mail.screen.SentPage;
import org.testng.Assert;
import org.testng.annotations.*;

public class SendMailPositiveTest extends BaseMailTest {

    @Test(description = "Sent a letter and check that it appears in Sent and Inbox folders")
    public void SendMailPositiveTest() {
        Letter letter = LetterBuilder.getTestLetter();
        letterService.sendLetter(letter);
        pause();
        Assert.assertTrue(letterService.isLetterInFolder(new SentPage(), letter));
        pause();
        Assert.assertTrue(letterService.isLetterInFolder(new InboxPage(), letter));
    }
}