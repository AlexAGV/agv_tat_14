package com.gomel.tat.home14.tests.disk;

import com.gomel.tat.home14.lib.feature.ydisk.TestFile;
import com.gomel.tat.home14.lib.feature.ydisk.TestFileFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

public class DeletePermanentTest extends BaseYDiskTest {

    TestFile testFile = TestFileFactory.getTestFile();

    @Test
    public void DeletePermanentTest() {
        testFile.storeTestFile();
        diskService.openRootFolder();
        diskService.uploadFile(testFile);
        testFile.deleteStoredTestFile();
        diskService.moveToTrash(testFile);
        diskService.openTrashBin();
        diskService.permanentDelete(testFile);
        pause();
        Assert.assertTrue(!diskService.isTestFilePresent(testFile));
    }
}
