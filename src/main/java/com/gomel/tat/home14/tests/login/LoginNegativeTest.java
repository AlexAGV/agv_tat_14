package com.gomel.tat.home14.tests.login;

import com.gomel.tat.home14.lib.common.Account;
import com.gomel.tat.home14.lib.common.AccountBuilder;
import com.gomel.tat.home14.lib.feature.mail.service.LoginService;
import org.testng.Assert;
import org.testng.annotations.Test;

public class LoginNegativeTest extends BaseLoginTest {

    @Test(description = "Login to mailbox with invalid password")
    public void LoginNegativeTest() {
        Account account = AccountBuilder.getAccountWithInvalidPassword();
        LoginService loginService = new LoginService();
        Assert.assertTrue(loginService.checkErrorWithWrongLogin(account));
    }
}
