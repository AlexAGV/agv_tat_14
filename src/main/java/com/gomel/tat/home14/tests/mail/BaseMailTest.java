package com.gomel.tat.home14.tests.mail;

import com.gomel.tat.home14.lib.feature.mail.service.LetterService;
import com.gomel.tat.home14.lib.feature.mail.service.LoginService;
import com.gomel.tat.home14.lib.ui.Browser;
import com.gomel.tat.home14.lib.util.Logger;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;

public class BaseMailTest {

    LetterService letterService = new LetterService();

    @BeforeSuite
    public void prepeareForTests() {
        Logger.debug("Beforesuite rises browser");
        if (!Browser.isBrowserOpened()) Browser.rise();
        LoginService loginService = new LoginService();
        loginService.loginToMailBox();
        pause();
    }

    @AfterSuite
    public void closeWebdriver() {
        Logger.debug("Closing browser after Mail tests");
          Browser.current().quit();
    }

    protected void pause() {
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
        }
    }
}
