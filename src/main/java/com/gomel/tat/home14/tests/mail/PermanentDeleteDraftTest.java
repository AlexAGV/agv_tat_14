package com.gomel.tat.home14.tests.mail;

import com.gomel.tat.home14.lib.feature.mail.Letter;
import com.gomel.tat.home14.lib.feature.mail.LetterBuilder;
import com.gomel.tat.home14.lib.feature.mail.screen.DraftPage;
import com.gomel.tat.home14.lib.feature.mail.screen.TrashPage;
import com.gomel.tat.home14.lib.util.Logger;
import org.testng.Assert;
import org.testng.annotations.Test;

public class PermanentDeleteDraftTest extends BaseMailTest {

    @Test(description = "Create a draft, delete it from Draft folder, delete it from Trash folder and check that it disappears in Trash folder")
    public void PermanentDeleteDraftTest() {
        Letter letter = LetterBuilder.getTestLetter();
        letterService.createDraft(letter);
        letterService.deleteLetter(new DraftPage(), letter);
        pause();
        letterService.deleteLetter(new TrashPage(), letter);
        pause();
        Assert.assertTrue(!letterService.isLetterInFolder(new TrashPage(), letter));
    }
}
