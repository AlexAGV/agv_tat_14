package com.gomel.tat.home14.tests.mail;

import com.gomel.tat.home14.lib.feature.mail.Letter;
import com.gomel.tat.home14.lib.feature.mail.LetterBuilder;
import com.gomel.tat.home14.lib.feature.mail.screen.InboxPage;
import com.gomel.tat.home14.lib.feature.mail.screen.SentPage;
import org.testng.Assert;
import org.testng.annotations.Test;

public class SendLetterWithoutSubjectTest extends BaseMailTest {

    @Test(description = "Sent a letter without subject and check that it appears in Sent and Inbox folders", timeOut = 15000)
    public void SendLetterWithoutSubjectTest() {
        Letter letter = LetterBuilder.getLetterWithoutSubj();
        letter.setMessageID(letterService.sendLetterReturnId(letter));
        letterService.waitForIncoming();
        Assert.assertTrue(letterService.isLetterInFolderWithId(new SentPage(), letter));
        pause();
        Assert.assertTrue(letterService.isLetterInFolderWithId(new InboxPage(), letter));
    }
}
