package com.gomel.tat.home14.tests.login;

import com.gomel.tat.home14.lib.common.Account;
import com.gomel.tat.home14.lib.common.AccountBuilder;
import com.gomel.tat.home14.lib.feature.mail.service.LoginService;
import org.testng.Assert;
import org.testng.annotations.Test;


public class LoginPositiveTest extends BaseLoginTest {

    @Test(description = "Login to mailbox as a valid password")
    public void LoginPositiveTest() {
        Account account = AccountBuilder.getDefaultAccount();
        LoginService loginService = new LoginService();
        Assert.assertTrue(loginService.checkLoginSuccessfull(account));
    }
}
