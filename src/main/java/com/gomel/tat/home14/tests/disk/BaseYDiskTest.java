package com.gomel.tat.home14.tests.disk;

import com.gomel.tat.home14.lib.feature.ydisk.service.DiskService;
import com.gomel.tat.home14.lib.feature.ydisk.service.YDLoginService;
import com.gomel.tat.home14.lib.ui.Browser;
import com.gomel.tat.home14.lib.util.Logger;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;

public class BaseYDiskTest {

    DiskService diskService = new DiskService();

    @BeforeSuite
    public void prepeareForTests() {
        Logger.debug("@before Yandex Disk tests");
        if (!Browser.isBrowserOpened()) {Browser.rise();}
        YDLoginService loginService = new YDLoginService();
        loginService.loginToYandexDisk();
        pause();
    }

    @AfterSuite
    public void closeWebdriver() {
        Logger.debug("Closing browser after Yandex Disk tests");
        Browser.current().quit();
    }

    protected void pause() {
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
        }
    }
}
