package com.gomel.tat.home14.tests.mail;

import com.gomel.tat.home14.lib.feature.mail.Letter;
import com.gomel.tat.home14.lib.feature.mail.LetterBuilder;
import org.testng.Assert;
import org.testng.annotations.*;

public class SendMailNegativeTest  extends BaseMailTest {

    @Test(description = "Sent a letter with invalid address and check that Error Message appears")
    public void SendMailNegativeTest() {
        Letter letter = LetterBuilder.getLetterWithWrongAddress();
        letterService.sendLetter(letter);
        Assert.assertTrue(letterService.isErrorWhileSend());
        letterService.cancelCompose();
    }
}
